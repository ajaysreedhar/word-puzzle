package in.sree.puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.sree.puzzle.gen.Position;
import in.sree.puzzle.gen.PuzzleBoard;
import in.sree.puzzle.util.StringListUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PuzzleBoardTest {

    private List<String> words;

    @Before
    public void setUp() throws Exception {
        words = Arrays.asList("AJAX", "ASBESTOS", "TORSION", "PRACTICALJOKE", "ANIMATE", "ANTIPOPE", "SAKE", "SANDWICHBOARD");
    }


    @Test
    public void testPuzzleBoard() throws Exception {
        int size = 13;
        words = StringListUtils.sortDescending(words);

        PuzzleBoard board = new PuzzleBoard(size);
        char[][] matrix = board.populate(words);

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                System.out.print(matrix[row][col]);
                System.out.print(' ');
            }

            System.out.print("\n");
        }
    }

    @Test
    public void testSort() throws Exception {
        List<Position> positions = new ArrayList<>();

        positions.add(new Position(0,0, 2, 1, "Score1"));
        positions.add(new Position(0,0, 2, 0, "Score2"));
        positions.add(new Position(0,0, 2, 2, "Score3"));

        positions.sort((position, t1) -> t1.getScore() - position.getScore());
        Assert.assertEquals("[(0, 0) 2: VERTICAL - Score3, (0, 0) 1: VERTICAL - Score1, (0, 0) 0: VERTICAL - Score2]",
                positions.toString());
    }
}
