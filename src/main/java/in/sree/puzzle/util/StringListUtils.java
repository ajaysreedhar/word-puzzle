package in.sree.puzzle.util;
import java.util.List;

public class StringListUtils {

    public static List<String> sortDescending(List<String> list) {
        list.sort((s, t) -> t.length() - s.length());

        return list;
    }
}
