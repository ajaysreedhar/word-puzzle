package in.sree.puzzle;

import java.util.ArrayList;
import java.util.List;

import in.sree.puzzle.util.StringListUtils;
import in.sree.puzzle.gen.PuzzleBoard;

/**
 * Main class.
 */
public class WordPuzzle {

    /**
     * Reads the inputs and builds the puzzle board.
     *
     * @param args command line arguments ignored in this version
     */
    public static void main(String[] args) {
        String word;
        int size;
        List<String> words = new ArrayList<>();

        try {
            System.out.print("Grid Size: ");
            size = Integer.parseInt(System.console().readLine());

        } catch (NumberFormatException e) {
            System.err.println("Error: The input is not am integer.");
            return;
        }

        System.out.print("Words for the puzzle (Empty string to stop): ");

        while (true) {
            word = System.console().readLine();

            if (word.equals("")) break;

            /* Trim trailing white spaces. */
            word = word.trim();

            if (word.length() > size) {
                System.err.println("Error: " + word + ", length is greater than grid size.");
                continue;
            }

            /* Converts to upper case and adds to the list. */
            words.add(word.toUpperCase());
        }

        words = StringListUtils.sortDescending(words);

        PuzzleBoard board = new PuzzleBoard(size);
        char[][] matrix = board.populate(words);

        /* Prints the puzzle matrix. */
        System.out.print("\n");
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                System.out.print(matrix[row][col]);
                System.out.print(' ');
            }

            System.out.print("\n");
        }

        /* Prints the words placed. */
        System.out.println("\n\nWords placed:\n");

        for (String w : board.getPlacedWords()) {
            System.out.println(w);
        }
    }
}
