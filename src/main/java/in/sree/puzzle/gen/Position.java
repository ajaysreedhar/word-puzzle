package in.sree.puzzle.gen;

/**
 * Identifies a cell in the board with row number,
 * column number, a word and direction.
 */
public class Position {

    /**
     * Integer code for horizontal direction.
     */
    static final int HORIZONTAL = 1;

    /**
     * Integer code for vertical direction.
     */
    static final int VERTICAL = 2;

    /**
     * Row number of the cell.
     */
    private int row;

    /**
     * Column number of the cell.
     */
    private int col;

    /**
     * Direction to which the word is to be placed.
     */
    private int direction;

    /**
     * The score of the word at the given position.
     */
    private int score;

    /**
     * The word.
     */
    private String word;

    /**
     * Initializes member variables from the given parameters.
     *
     * @param row the row number of the cell
     * @param col the column number of the cell
     * @param direction the direction of the word
     * @param word the word to be placed
     */
    public Position(int row, int col, int direction, int score, String word) {
        this.row = row;
        this.col = col;
        this.direction = direction;
        this.word = word;
        this.score = score;
    }

    /**
     * Sets the score of the word at the given position.
     *
     * @param score the score of the word
     */
    void setScore(int score) {
        this.score = score;
    }

    /**
     * Returns the row number of the cell.
     *
     * @return row number
     */
    int getRow() {
        return row;
    }

    /**
     * Returns the column number of the cell.
     *
     * @return column number
     */
    int getCol() {
        return col;
    }

    /**
     * Returns the word direction.
     *
     * @return direction
     */
    int getDirection() {
        return direction;
    }

    /**
     * Returns the word.
     *
     * @return word
     */
    String getWord() {
        return word;
    }

    /**
     * Returns the score at the given position.
     *
     * @return word score
     */
    public int getScore() {
        return score;
    }

    /**
     * Print the data as a string.
     *
     * @return to string
     */
    @Override
    public String toString() {
        return "(" + row + ", " + col + ") " + getScore() + ": "
            + ((direction == VERTICAL) ? "VERTICAL" : "HORIZONTAL") + " - " + word;
    }
}
