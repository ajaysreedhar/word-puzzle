package in.sree.puzzle.gen;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds the board as a matrix and associated methods.
 */
public class PuzzleBoard {

    /**
     * The blank character on the board.
     *
     * <p>Can be changed to improve visual appeal of the board.</p>
     */
    private static final char BLANK = '-';

    /**
     * Board size as an integer.
     *
     * The board will be a square with <i>size</i> number of
     * rows and <i>size</i> number of columns.
     */
    private int size;

    /**
     * The puzzle board as a matrix.
     */
    private char[][] board;

    /**
     * A list of words successfully placed on the board.
     */
    private List<String> placedWords;

    /**
     * Places a given word on the board starting from a given cell to a given direction.
     *
     * @param row the row number
     * @param col the column number
     * @param direction horizontal or vertical direction
     * @param word the word to be placed
     */
    private void placeAtPosition(int row, int col, int direction, String word) {
        for (char letter : word.toCharArray()) {
            board[row][col] = letter;

            if (direction == Position.VERTICAL) row++;
            else col++;
        }
    }

    /**
     * Places a given word on the board starting from a given cell to a given direction.
     *
     * @param position the position of the word
     */
    private void placeAtPosition(Position position) {
        placeAtPosition(position.getRow(), position.getCol(), position.getDirection(), position.getWord());
    }

    /**
     * Sorts the suggested positions by score in descending order.
     *
     * <p>The method removes all position with a score 0.</p>
     *
     * @param positions the {@link List} of suggested positions
     * @return sorted position list
     */
    private List<Position> sortByScore(List<Position> positions) {
        List<Position> filtered = new ArrayList<>();
        int score;

        for (Position position : positions) {
            score = findScore(position.getRow(), position.getCol(), position.getDirection(), position.getWord());

            if (score <= 0) continue;

            position.setScore(score);
            filtered.add(position);
        }

        filtered.sort((s, t) -> t.getScore() - s.getScore());
        return filtered;
    }

    /**
     * Finds suggested positions, and direction and returns the list.
     *
     * @param word the word to be placed.
     * @return {@link List} of suggested positions
     */
    private List<Position> findSuggestions(String word) {
        int length = word.length(), index = -1, row, col;
        List<Position> positions = new ArrayList<>();

        for (char letter : word.toCharArray()) {
            index++;

            for (row = 0; row < size; row++) {
                for (col = 0; col < size; col++) {
                    if (letter == board[row][col]) {
                        try {
                            if (row - index >= 0) {
                                if ((row - index) + length <= size ) {
                                    positions.add(new Position(row - index, col, Position.VERTICAL, 0, word));
                                }
                            }

                            if ((col - index) >= 0) {
                                if ((col - index) + length <= size) {
                                    positions.add(new Position(row, col - index, Position.HORIZONTAL, 0, word));
                                }
                            }

                        } catch (IndexOutOfBoundsException ignored) {}

                    } else if (BLANK == board[row][col]) {
                        try {
                            if (BLANK == board[row+1][col] && BLANK == board[row][col+1]) {
                                if ((size - row) >= length)
                                    positions.add(new Position(row, col, Position.VERTICAL, 0, word));

                                else if ((size - col) >= length)
                                    positions.add(new Position(row, col, Position.HORIZONTAL, 0, word));
                            }
                        } catch (IndexOutOfBoundsException ignored) {}
                    }
                }
            }
        }

        positions = sortByScore(positions);
        return positions;
    }

    /**
     * Finds fitness score for a word to be placed at a cell
     * identified by row number and column number.
     *
     * <p>Returns 0 if there is no fit, 1 if fit and 2 if match.</p>
     *
     * @param row the row number
     * @param col the column number
     * @param direction horizontal or vertical direction
     * @param word the word
     * @return score at given position
     */
    private int findScore(int row, int col, int direction, String word) {
        if (col < 0 || row < 0) return 0;

        int count = 1, score = 1;
        char cell;

        for (char letter : word.toCharArray()) {
            try {
                cell = board[row][col];

            } catch (IndexOutOfBoundsException e) {
                return 0;
            }

            if (cell != BLANK && cell != letter) {
                return 0;

            } else if (cell == letter) {
                score++;
            }

            if (direction == Position.VERTICAL) {
                if (cell != letter) {
                    if (!isClear(row, col + 1)) return 0;
                    if (!isClear(row, col - 1)) return 0;
                }

                if (count == 1) {
                    if (!isClear(row - 1, col)) return 0;
                }

                if (count == word.length()) {
                    if (!isClear(row + 1, col)) return 0;
                }

            } else {
                if (cell != letter) {
                    if (!isClear(row - 1, col)) return 0;
                    if (!isClear(row + 1, col)) return 0;
                }

                if (count == 1) {
                    if (!isClear(row, col - 1)) return 0;
                }

                if (count == word.length()) {
                    if (!isClear(row, col + 1)) return 0;
                }
            }

            if (direction == Position.VERTICAL) row++;
            else col++;

            count++;
        }

        return score;
    }

    /**
     * Checks if a cell is clear.
     *
     * @param row the row number of the cell
     * @param col the column number of the cell
     * @return true if clear, false otherwise
     */
    private boolean isClear(int row, int col) {
        try {
            return board[row][col] == BLANK;

        } catch (IndexOutOfBoundsException e) {
            return true;
        }
    }

    /**
     * Places a word on the board on a matching position.
     *
     * @param word the word to be placed
     * @return true if the word is placed, false otherwise
     */
    private boolean placeWord(String word) {
        int count = 0, direction;

        Position position;
        List<Position> positions = (0 == placedWords.size()) ? new ArrayList<>() : findSuggestions(word);

        while (count < ((size -1) * (size - 1))) {
            if (0 == placedWords.size()) {
                direction = (Math.random() <= 0.5) ? Position.HORIZONTAL : Position.VERTICAL;

                if (0 < findScore(0, 0, direction, word)) {
                    placeAtPosition(0, 0, direction, word);
                    return true;
                }

            } else {
                try {
                    position = positions.get(count);

                    if (0 < position.getScore()) {
                        placeAtPosition(position);
                        return true;
                    }
                } catch (IndexOutOfBoundsException ignored) {
                    return false;
                }
            }

            count++;
        }

        return false;
    }

    /**
     * Initializes board size and board.
     *
     * @param size the board size
     */
    public PuzzleBoard(int size) {
        this.size = size;

        board = new char[size][size];
        placedWords = new ArrayList<>();

        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                board[row][col] = BLANK;
            }
        }
    }

    /**
     * Populates the board with the words forming crosswords.
     *
     * @param words a {@link List} of words to be placed on the board
     * @return the board
     */
    public char[][] populate(List<String> words) {
        for (String word : words) {

            if (placeWord(word)) {
                placedWords.add(word);
            }
        }

        return board;
    }

    /**
     * Returns the list of words placed.
     *
     * @return words placed
     */
    public List<String> getPlacedWords() {
        return placedWords;
    }
}
