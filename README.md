# Word Puzzle

A simple java application which attempts to populate a given list of words on a puzzle matrix of size n*n.

## How to install

- Requires Java 1.8

Clone the repository
```shell
$ git clone https://ajaysreedhar@bitbucket.org/ajaysreedhar/word-puzzle.git
$ cd word-puzzle
```

Build with gradle

_On Linux and Mac OS:_
```shell
$ ./gradlew clean shadowJar
```

_On Windows:_
```shell
./gradlew.bat clean shadowJar
```

Run the application
```shell
$ java -jar build/libs/word-puzzle-1.0-SNAPSHOT-all.jar
```

### Input sample
```shell
$ java -jar build/libs/word-puzzle-1.0-SNAPSHOT-all.jar

Grid Size: 13
Words for the puzzle (Empty string to stop): SANDWICHBOARD
PRACTICALJOKE
TREMOLO
QATAR
TORSION
WILLOW

S A N D W I C H B O A R D
- - - - - - - - - - - - -
P R A C T I C A L J O K E
- - - - R - - - - - - - -
- - - - E - - - - - - Q -
- - - - M - - - - - - A -
- - - T O R S I O N - T -
- - - - L - - - - - - A -
W I L L O W - - - - - R -
- - - - - - - - - - - - -
- - - - - - - - - - - - -
- - - - - - - - - - - - -
- - - - - - - - - - - - -
```
